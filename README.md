# Role-Playing-Game Character Creations

S.O.L.I.D. refractoring of existing project.

## Design choices:
- Since physical damage dealt is calculated in the same way regardless of weapon type, a single function is used for this.
- The documentation specifies that Priest use shield magic and Druids heal, therefore no common Support class has been made as it would contain no additional functionality.

## Character inheritance UML diagram
Following diagram shows how the classes has been refractored:
![UML of Hero Inheritance](Character%20inheritance.png)