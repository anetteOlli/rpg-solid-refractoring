package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.MagicWeapon;

public class Staff implements MagicWeapon {

    // Rarity
    private Rarity rarity;

    // Public properties
    @Override
    public Rarity getItemRarity() {
        return rarity;
    }

    @Override
    public double getMagickPowerModifier() {
        return WeaponStatsModifiers.STAFF_MAGIC_MOD;
    }

    // Constructors
    public Staff() {
        this.rarity = new Common();
    }

    public Staff(Rarity rarity) {
        this.rarity = rarity;
    }
}
