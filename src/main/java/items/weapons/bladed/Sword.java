package main.java.items.weapons.bladed;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Sword implements BladeWeapon {

    // Rarity
    private Rarity rarity;

    // Public properties
    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.SWORD_ATTACK_MOD;
    }

    @Override
    public Rarity getItemRarity() {
        return rarity;
    }

    // Constructors
    public Sword() {
        this.rarity = new Common();
    }

    public Sword(Rarity rarity) {
        this.rarity = rarity;
    }


}
