package main.java.items.weapons.bladed;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BladeWeapon;

public class Dagger implements BladeWeapon {

    // Rarity
    private Rarity rarity;

    // Public properties

    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.DAGGER_ATTACK_MOD;
    }

    @Override
    public Rarity getItemRarity() {
        return rarity;
    }

    // Constructors
    public Dagger() {
        this.rarity = new Common();
    }

    public Dagger(Rarity rarity) {
        this.rarity = rarity;
    }

}
