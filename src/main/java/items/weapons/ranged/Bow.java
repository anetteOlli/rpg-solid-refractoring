package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Bow implements RangedWeapon {

    // Rarity
    private Rarity rarity;

    // Public properties
    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.BOW_ATTACK_MOD;
    }

    @Override
    public Rarity getItemRarity() {
        return rarity;
    }

    // Constructors
    public Bow() {
        this.rarity = new Common();
    }

    public Bow(Rarity rarity) {
        this.rarity = rarity;
    }


}
