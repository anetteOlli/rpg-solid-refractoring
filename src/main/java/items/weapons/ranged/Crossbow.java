package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Crossbow implements RangedWeapon {
    // Stat modifiers
    private double attackPowerModifier = WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
    // Rarity
    private Rarity rarity;

    // Public properties
    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.CROSSBOW_ATTACK_MOD;
    }

    @Override
    public Rarity getItemRarity() {
        return rarity;
    }

    // Constructors
    public Crossbow() {
        this.rarity = new Common();
    }

    public Crossbow(Rarity rarity) {
        this.rarity = rarity;
    }


}
