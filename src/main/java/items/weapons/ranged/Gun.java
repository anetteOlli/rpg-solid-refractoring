package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.RangedWeapon;

public class Gun implements RangedWeapon {

    // Rarity
    private Rarity rarity;

    // Public properties
    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.GUN_ATTACK_MOD;
    }

    @Override
    public Rarity getItemRarity() {
        return rarity;
    }

    // Constructors
    public Gun() {
        this.rarity = new Common();
    }

    public Gun(Rarity rarity) {
        this.rarity = rarity;
    }


}
