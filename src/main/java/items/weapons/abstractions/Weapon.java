package main.java.items.weapons.abstractions;

import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;

public abstract interface Weapon {

    Rarity getItemRarity();
}
