package main.java.items.weapons.blunted;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.rarity.Common;
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.BluntWeapon;

public class Mace implements BluntWeapon {

    // Rarity
    private Rarity rarity;

    // Public properties
    @Override
    public double getAttackPowerModifier() {
        return WeaponStatsModifiers.MACE_ATTACK_MOD;
    }

    @Override
    public Rarity getItemRarity() {
        return rarity;
    }

    // Constructors
    public Mace() {
        this.rarity = new Common();
    }

    public Mace(Rarity rarity) {
        this.rarity = rarity;
    }


}
