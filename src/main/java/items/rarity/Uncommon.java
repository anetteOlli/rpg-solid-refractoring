package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Uncommon implements Rarity {

    @Override
    public double getPowerModifier() {
        return 1.2;
    }
    @Override
    public String getItemRarityColor() {
        return Color.GREEN;
    }
}
