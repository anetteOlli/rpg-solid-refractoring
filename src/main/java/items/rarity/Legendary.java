package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Legendary implements Rarity {

    @Override
    public double getPowerModifier() {
        return 2;
    }
    @Override
    public String getItemRarityColor() {
        return Color.YELLOW;
    }
}
