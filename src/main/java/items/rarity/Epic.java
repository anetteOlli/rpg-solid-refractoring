package main.java.items.rarity;

import main.java.consolehelpers.Color;
import main.java.items.rarity.abstractions.Rarity;

public class Epic implements Rarity {

    @Override
    public double getPowerModifier() {
        return 1.6;
    }
    @Override
    public String getItemRarityColor() {
        return Color.MAGENTA;
    }
}
