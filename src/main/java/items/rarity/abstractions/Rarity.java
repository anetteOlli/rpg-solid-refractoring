package main.java.items.rarity.abstractions;

public interface Rarity {

    double getPowerModifier();

    String getItemRarityColor();


}
