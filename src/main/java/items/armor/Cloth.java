package main.java.items.armor;
// Imports
import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.ItemRarity;

public class Cloth implements Armor {

    // Rarity
    private final double rarityModifier;

    // Public properties
    @Override
    public double getHealthModifier() {
        return ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER;
    }

    @Override
    public double getPhysRedModifier() {
        return ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER;
    }

    @Override
    public double getMagicRedModifier() {
        return ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

    // Constructors
    public Cloth(double itemRarity) {
        this.rarityModifier = itemRarity;
    }

}
