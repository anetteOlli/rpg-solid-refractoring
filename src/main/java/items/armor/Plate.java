package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.ItemRarity;

public class Plate implements Armor {

    // Rarity
    private final double rarityModifier;

    // Public properties
    @Override
    public double getHealthModifier() {
        return ArmorStatsModifiers.PLATE_HEALTH_MODIFIER;
    }

    @Override
    public double getPhysRedModifier() {
        return ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER;
    }

    @Override
    public double getMagicRedModifier() {
        return ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

    // Constructors
    public Plate(double itemRarity) {
        this.rarityModifier = itemRarity;
    }
}
