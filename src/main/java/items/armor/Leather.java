package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.ItemRarity;

public class Leather implements Armor {

    // Rarity
    private final double rarityModifier;

    // Public properties
    @Override
    public double getHealthModifier() {
        return ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER;
    }

    @Override
    public double getPhysRedModifier() {
        return ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER;
    }

    @Override
    public double getMagicRedModifier() {
        return ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

    // Constructors
    public Leather(double itemRarity) {
        this.rarityModifier = itemRarity;
    }
}
