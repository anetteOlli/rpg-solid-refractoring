package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.rarity.abstractions.ItemRarity;

public class Mail implements Armor {

    // Rarity
    private final double rarityModifier;

    // Public properties
    @Override
    public double getHealthModifier() {
        return ArmorStatsModifiers.MAIL_HEALTH_MODIFIER;
    }

    @Override
    public double getPhysRedModifier() {
        return ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER;
    }

    @Override
    public double getMagicRedModifier() {
        return ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER;
    }

    @Override
    public double getRarityModifier() {
        return rarityModifier;
    }

    // Constructors
    public Mail(double itemRarity) {
        this.rarityModifier = itemRarity;
    }
}
