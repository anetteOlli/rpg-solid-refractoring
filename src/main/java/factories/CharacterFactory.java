package main.java.factories;
// Imports
import main.java.characters.*;
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Hero;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.abstractions.SpellType;


/*
 This factory exists to be responsible for creating new characters.
 Object is replaced with Character as a return type when refactored to be good OO design.
*/
public class CharacterFactory {
    public Hero getCharacter(CharacterType characterType) {
        switch(characterType) {
            case Druid:
                return new Druid( (HealingSpell) new SpellFactory().getSpell(SpellType.Regrowth) );
            case Mage:
                return new Mage( (DamagingSpell) new SpellFactory().getSpell(SpellType.ArcaneMissile) );
            case Paladin:
                return new Paladin();
            case Priest:
                return new Priest((ShieldingSpell) new SpellFactory().getSpell(SpellType.Rapture));
            case Ranger:
                return new Ranger();
            case Rogue:
                return new Rogue();
            case Warlock:
                return new Warlock( (DamagingSpell) new SpellFactory().getSpell(SpellType.ChaosBolt) );
            case Warrior:
                return new Warrior();
            default:
                return null;
        }
    }
}
