package main.java.factories;
// Imports
import main.java.items.rarity.abstractions.ItemRarity;
import main.java.items.rarity.abstractions.Rarity;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.bladed.Axe;
import main.java.items.weapons.bladed.Dagger;
import main.java.items.weapons.bladed.Sword;
import main.java.items.weapons.blunted.Hammer;
import main.java.items.weapons.blunted.Mace;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;
/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Weapon as a return type when refactored to be good OO design.
*/
public class WeaponFactory {
    public Weapon getItem(WeaponType weaponType, Rarity rarity) {
        switch(weaponType) {
            case Axe:
                return new Axe(rarity);
            case Bow:
                return new Bow(rarity);
            case Crossbow:
                return new Crossbow(rarity);
            case Dagger:
                return new Dagger(rarity);
            case Gun:
                return new Gun(rarity);
            case Hammer:
                return new Hammer(rarity);
            case Mace:
                return new Mace(rarity);
            case Staff:
                return new Staff(rarity);
            case Sword:
                return new Sword(rarity);
            case Wand:
                return new Wand(rarity);
            default:
                return null;
        }
    }

    public Weapon getCommonWeapon( WeaponType weaponType ) {
        switch ( weaponType ) {
            case Axe:
                return new Axe();
            case Bow:
                return new Bow();
            case Crossbow:
                return new Crossbow();
            case Dagger:
                return new Dagger();
            case Gun:
                return new Gun();
            case Hammer:
                return new Hammer();
            case Mace:
                return new Mace();
            case Staff:
                return new Staff();
            case Sword:
                return new Sword();
            case Wand:
                return new Wand();
            default:
                return null;
        }
    }
}
