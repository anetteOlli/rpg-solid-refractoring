package main.java.characters.abstractions;

import main.java.consolehelpers.Color;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.spells.abstractions.HealingSpell;

public abstract class HealingSupport extends Hero {

    private HealingSpell healingSpell;

    public HealingSupport(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, HealingSpell healingSpell , String heroName){
        super(baseHealth,
                basePhysReductionPercent,
                baseMagicReductionPercent,
                heroName);
        this.healingSpell = healingSpell;
    }

    public HealingSupport(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent , String heroName){
        super(baseHealth,
                basePhysReductionPercent,
                baseMagicReductionPercent,
                heroName);
    }


    public HealingSpell getHealingSpell() {
        return healingSpell;
    }

    public void setHealingSpell(HealingSpell healingSpell) {
        this.healingSpell = healingSpell;
    }

    //Behavior:

    /**
     *
     * @param target that will be healed
     * @return healingAmount that the target will receive. If no healingsSpell or weapon is equipped the healed amount will be 0.
     */
   public double healPartyMember(Hero target) {
       if (super.getDead()) {
           System.out.println(getHeroName() + " is dead and can't do anything");
           return 0;
       }
       double healing = 0;
       if ( healingSpell != null && super.getWeapon() != null ) {
           healing = healingSpell.getHealingAmount() * ((MagicWeapon) super.getWeapon()).getMagickPowerModifier() * super.getWeapon().getItemRarity().getPowerModifier();
           if (!target.getDead()) {
               System.out.println( Color.GREEN + super.getHeroName().toUpperCase() + " healed " + target.getHeroName().toUpperCase() + " for " + healing + " healing points" + Color.RESET );
               target.getHealed( healing );
           }
       }
       return healing;
   }
}
