package main.java.characters.abstractions;

public enum DamageType {
    physical,
    magical
}
