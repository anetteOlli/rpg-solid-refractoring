package main.java.characters.abstractions;

import main.java.enemies.abstractions.Enemy;
import main.java.items.weapons.abstractions.PhysicalWeapon;

public abstract class PhysicalDamageDealer extends Hero {

    private double baseAttackPower;

    public PhysicalDamageDealer(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double  baseAttackPower, String heroName) {
        super(baseHealth,
                basePhysReductionPercent,
                baseMagicReductionPercent,
                heroName);
        this.baseAttackPower = baseAttackPower;
    }


    /**
     * For pvp / friendly fire
     * @param target that will receive damage.
     * @return damage dealt
     */
    public double attackWithWeapon(Hero target) {
        if (super.getDead()) {
            return 0;
        }
        double damage = 0;
        if (super.getWeapon() != null) {
            PhysicalWeapon bluntWeapon = (PhysicalWeapon) super.getWeapon();
            damage = baseAttackPower * bluntWeapon.getAttackPowerModifier() * bluntWeapon.getItemRarity().getPowerModifier();
        }
        System.out.println(super.getHeroName().toUpperCase() + " dealt " + damage + " to " + target.getHeroName().toUpperCase());
        target.takeDamage(damage, DamageType.physical);

        return damage;
    }

    /**
     *
     * @param target that will receive damage.
     * @return damage dealt.
     */
    public double attackWithWeapon(Enemy target) {
        if (super.getDead()) {
            System.out.println(getHeroName() + " is dead and can't do anything");
            return 0;
        }
        double damage = 0;
        if (super.getWeapon() != null) {
            PhysicalWeapon physicalWeapon = (PhysicalWeapon) super.getWeapon();
            damage = baseAttackPower * physicalWeapon.getAttackPowerModifier() * physicalWeapon.getItemRarity().getPowerModifier();
        }

        System.out.println(super.getHeroName().toUpperCase() + " dealt " + damage + " to enemy");

        //TODO: implement enemy receiving damage.
        return damage;
    }
}
