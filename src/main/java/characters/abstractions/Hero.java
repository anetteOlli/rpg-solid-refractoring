package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.weapons.abstractions.Weapon;
import main.java.consolehelpers.Color;


public abstract class Hero {
    // Base stats defensive
    private double baseHealth;

    // Active trackers and flags
    private double currentHealth;
    private double basePhysReductionPercent; // Armor
    private double baseMagicReductionPercent; // Magic armor
    private Boolean isDead;
    private double shieldAmount;
    private final String heroName;

    //Equipment
    private Armor armor;
    private Weapon weapon;

    public Hero(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, String heroName) {
        this.baseHealth = baseHealth;
        this.currentHealth = baseHealth;
        this.isDead = false;
        this.basePhysReductionPercent = basePhysReductionPercent;
        this.baseMagicReductionPercent = baseMagicReductionPercent;
        this.shieldAmount = 0;
        this.heroName = heroName;

    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth() {
        double currentMaxHealth = 0;
        if (armor != null) {
            currentMaxHealth = baseHealth * armor.getHealthModifier() * armor.getRarityModifier();
        }
        return currentMaxHealth;
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public Boolean getDead() {
        return isDead;
    }
    public String getHeroName() {
        return heroName;
    }

    private void died() {
        isDead = true;
        System.out.println( getHeroName() + ": help me, I need hea..." + Color.RED + "\n" + heroName.toUpperCase() + " DIED" + Color.RESET);
    }

    public Armor getArmor() {
        return armor;
    }
    public double getShieldAmount() {
        return shieldAmount;
    }

    /**
     * Can only receive shield up to currentMaxHealth
     * @param shieldAmountReceived that will increase shieldAmount.
     * @throws IllegalArgumentException if shieldAmountReceived is negative.
     */
    public void increaseShield(double shieldAmountReceived) throws IllegalArgumentException {
        if( shieldAmountReceived >= 0) {
            if (shieldAmountReceived + shieldAmount > getCurrentMaxHealth() ) {
                shieldAmount = getCurrentMaxHealth();
            } else {
                shieldAmount += shieldAmountReceived;
            }
            System.out.println( Color.BLUE + getHeroName().toUpperCase() + " got shielded. Shield is now: " + shieldAmount + Color.RESET);
        } else {
            throw new IllegalArgumentException("shieldAmountReceived cannot have negative value");
        }

    }

    /**
     * Only child classes should be able to setArmor
     * @param armor that is equipped by the Hero
     */
    protected void setArmor(Armor armor) {
        this.armor = armor;
    }

    public abstract void equipArmor(Armor armor) throws IllegalArgumentException;

    public Weapon getWeapon() {
        return weapon;
    }

    /**
     * Only child classes should be able to setWeapons
     * @param weapon that is equipped by the Hero
     */
    protected void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public abstract void equipWeapon(Weapon weapon) throws IllegalArgumentException;


    /**
     * Sets the current health and shieldAmount based on damage received.
     * @param incomingDamage - amount of base damage the entity receives
     * @param damageType - physical or magical damageType
     * @return damage - the damage the character reduces it health by. Reduced by armor and shieldAmount.
     */
    public double takeDamage(double incomingDamage, DamageType damageType) {
        if (isDead) {
            System.out.println(getHeroName() + " is already dead and can't suffer anymore ");
            return 0;

        }

        //dampen damage from shieldAmount:
        double[] dampen = dampenDamage(incomingDamage, shieldAmount);
        double damage = dampen[0];
        shieldAmount = dampen[1];

        switch (damageType) {
            case magical:
                double armorMagicReductPercent = 0;
                double armorRarityModifier = 0;
                if ( armor != null ) {
                    armorMagicReductPercent = armor.getMagicRedModifier();
                    armorRarityModifier = armor.getRarityModifier();
                }
                damage = damage *( 1 - baseMagicReductionPercent * armorMagicReductPercent * armorRarityModifier );
            break;

            case physical:
                double armorPhysReductPercent = 0;
                armorRarityModifier = 0;
                if( armor != null) {
                    armorPhysReductPercent = armor.getPhysRedModifier();
                    armorRarityModifier = armor.getRarityModifier();
                }
                damage = damage * ( 1 - basePhysReductionPercent * armorPhysReductPercent * armorRarityModifier  );
            break;
            default: break;

        }

        //reduce currentHealth
        if (damage >= currentHealth) {
            currentHealth = 0;
            died();
        } else {
            currentHealth -= damage;
            System.out.println(Color.RED + this.getHeroName().toUpperCase() + " took " + damage + " incoming damage. Current health is " + currentHealth  + Color.RESET);
        }
        return damage;
    }


    /**
     * Calculates damage and shieldAmount without creating side-effects.
     * @param damage before armor has played it's role
     * @param shieldAmount shieldAmount the character has
     * @return [damage, shieldAmount] after shieldDampening
     */
    private double[] dampenDamage(double damage, double shieldAmount) {
        double[] results = new double[2];
        if (damage >= shieldAmount ) {
            results[0] = damage - shieldAmount;
            results[1] = 0;
        } else {
            results[0] = 0;
            results[1] = shieldAmount - damage;
        }
        return results;
    }

    /**
     *  Used for increase the currentHealth by for instance healing.
     * @param healedAmount amount of healing received.
     */
    public void getHealed(double healedAmount) throws IllegalArgumentException {
        if ( healedAmount > 0) {
            if (currentHealth + healedAmount > baseHealth) {
                currentHealth = baseHealth;
            } else {
                currentHealth += healedAmount;
            }
            System.out.println(Color.GREEN + getHeroName() + " current health is now " + currentHealth + Color.RESET);
        } else {
            throw new IllegalArgumentException("healedAmount cannot have negative value");
        }
    }
}
