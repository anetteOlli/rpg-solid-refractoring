package main.java.characters.abstractions;

import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.spells.abstractions.ShieldingSpell;

public abstract class ShieldingSupport extends Hero {



    private ShieldingSpell shieldingSpell;

    /**
     * Constructor
     * @param baseHealth of character
     * @param basePhysReductionPercent of character
     * @param baseMagicReductionPercent of character
     * @param shieldingSpell shieldingSpell wielded by character
     * @param heroName of character
     */
    public ShieldingSupport(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, ShieldingSpell shieldingSpell , String heroName) {
        super(baseHealth,
                basePhysReductionPercent,
                baseMagicReductionPercent,
                heroName);
        this.shieldingSpell = shieldingSpell;

    }

    /**
     * Constructor that leaves the character without a ShieldingSpell
     * @param baseHealth
     * @param basePhysReductionPercent
     * @param baseMagicReductionPercent
     * @param heroName
     */
    public ShieldingSupport(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent , String heroName) {
        super(baseHealth,
                basePhysReductionPercent,
                baseMagicReductionPercent,
                heroName);
    }

    public ShieldingSpell getShieldingSpell() {
        return shieldingSpell;
    }

    public void setShieldingSpell(ShieldingSpell shieldingSpell) {
        this.shieldingSpell = shieldingSpell;
    }



    //Behavior:


    /**
     *  Calculates and cast shield onto target.
     * @param target that receives shield
     * @return shield amount that the target will receive
     */
    public double shieldPartyMember(Hero target) {
        double shieldAmount = 0;
        if (shieldingSpell != null && super.getWeapon() != null) {
            if ( shieldingSpell instanceof ShieldingSpell ) {
                shieldAmount =
                        (
                                target.getCurrentMaxHealth()
                                        * ((ShieldingSpell) shieldingSpell).getAbsorbShieldPercentage()
                        ) + (
                                ((MagicWeapon) super.getWeapon()).getMagickPowerModifier()
                                        * super.getWeapon().getItemRarity().getPowerModifier()
                        );
            }
        }
        System.out.println(getHeroName().toUpperCase() + " shielded " + target.getHeroName().toUpperCase() );
        target.increaseShield(shieldAmount);
        return shieldAmount;
    }
}
