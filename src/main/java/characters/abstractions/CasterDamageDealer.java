package main.java.characters.abstractions;

import main.java.enemies.abstractions.Enemy;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.spells.abstractions.DamagingSpell;

import static main.java.characters.abstractions.DamageType.magical;

public abstract class CasterDamageDealer extends Hero  {

    private DamagingSpell damagingSpell;
    private double baseMagicPower;

    public CasterDamageDealer(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double baseMagicPower, String heroName) {
        super (baseHealth,
                basePhysReductionPercent,
                baseMagicReductionPercent,
                heroName);
        this.baseMagicPower = baseMagicPower;
    }

    public CasterDamageDealer(double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double baseMagicPower, String heroName, DamagingSpell damagingSpell) {
        super (baseHealth,
                basePhysReductionPercent,
                baseMagicReductionPercent,
                heroName);
        this.baseMagicPower = baseMagicPower;
        this.damagingSpell = damagingSpell;
    }

    /**
     *
     * @param target that will receive damage
     * @return damage dealt
     */
    public double castDamagingSpell(Enemy target) {
        if (super.getDead()) {
            System.out.println(getHeroName() + " is dead and can't do anything");
            return 0;
        }
        double magicDamageDealt = 0;
        if( super.getWeapon() != null && damagingSpell != null) {
            magicDamageDealt = baseMagicPower * ( (MagicWeapon) super.getWeapon() ).getMagickPowerModifier() * damagingSpell.getSpellDamageModifier() ;
        }
        //TODO: Logic on enemies to receive damage.
        System.out.println(super.getHeroName().toUpperCase() + " dealt " + magicDamageDealt + " magic damage to enemy");
        return magicDamageDealt;
    }

    /**
     * For pvp / friendly fire mode
     * @param target that will receive the damage
     * @return damage dealt
     */
    public double castDamagingSpell(Hero target) {
        if (super.getDead()) {
            System.out.println(getHeroName() + " is dead and can't do anything");
            return 0;
        }
        double magicDamageDealt = 0;
        if( super.getWeapon() != null && damagingSpell != null) {
            magicDamageDealt = baseMagicPower * ( (MagicWeapon) super.getWeapon() ).getMagickPowerModifier() * damagingSpell.getSpellDamageModifier() ;
        }
        System.out.println( super.getHeroName().toUpperCase() + " dealt " + magicDamageDealt + " magic damage to " + target.getHeroName().toUpperCase() );
        target.takeDamage(magicDamageDealt, magical);

        return magicDamageDealt;
    }


}
