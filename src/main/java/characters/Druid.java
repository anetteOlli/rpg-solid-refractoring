package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.HealingSupport;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Leather;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.HealingSpell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends HealingSupport {


    // Constructor
    public Druid(HealingSpell healingSpell, String heroName) {
        super(CharacterBaseStatsDefensive.DRUID_BASE_HEALTH,
                CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES,
                healingSpell,
                heroName);
        equipArmor( new ArmorFactory().getArmor(ArmorType.Leather, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Staff));
    }

    public Druid(HealingSpell healingSpell) {
        super(CharacterBaseStatsDefensive.DRUID_BASE_HEALTH,
                CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES,
                healingSpell,
                "Tadg mac Nuadat");
        equipArmor( new ArmorFactory().getArmor(ArmorType.Leather, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Staff));
    }



    // Equipment behaviours

    /**
     * Equips armor to the character, used for calculating damage taken.
     * @param armor - Leather armor.
     * @throws IllegalArgumentException - if the armor is not an instance of Leather
     */
    @Override
    public void equipArmor( Armor armor) {
        if (armor instanceof Leather) {
            super.setArmor(armor);
        } else {
            throw new IllegalArgumentException("Required param: armor instance of Leather. Received:" + armor.getClass());
        }
    }

    /**
     * Equip weapon to the character. Weapons are later used for calculating healing amount.
     * @param weapon - must be of MagicWeapon
     * @throws IllegalArgumentException - if the weapon is not an instance of MagicWeapon
     */
    @Override
    public void equipWeapon(Weapon weapon) throws IllegalArgumentException {
        if ( weapon instanceof MagicWeapon ) {
            super.setWeapon(weapon);
        } else {
            throw new IllegalArgumentException("Required param: weapon instance of MagicWeapon. Recieved: " + weapon.getClass());
        }
    }

}
