package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CasterDamageDealer;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Cloth;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.DamagingSpell;


/*
 Class description:
 ------------------
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends CasterDamageDealer {



    // Constructor
    public Mage() {
        // When the character is created it has maximum health (base health)
        super(CharacterBaseStatsDefensive.MAGE_BASE_HEALTH,
                CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.MAGE_MAGIC_POWER,
                "Harry Potter");
        equipArmor( new ArmorFactory().getArmor(ArmorType.Cloth, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Staff));
    }

    public Mage(DamagingSpell damagingSpell, String heroName) {
        super(CharacterBaseStatsDefensive.MAGE_BASE_HEALTH,
                CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.MAGE_MAGIC_POWER,
                heroName ,
                damagingSpell);
        equipArmor( new ArmorFactory().getArmor(ArmorType.Cloth, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Staff));
    }

    public Mage(DamagingSpell damagingSpell) {
        super(CharacterBaseStatsDefensive.MAGE_BASE_HEALTH,
                CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.MAGE_MAGIC_POWER,
                "Harry Potter" ,
                damagingSpell);
        equipArmor( new ArmorFactory().getArmor(ArmorType.Cloth, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Staff));
    }



    // Equipment behaviours

    /**
     * Equips armor to the character, used for calculating damage taken.
     * @param armor - Cloth armor.
     * @throws IllegalArgumentException - if the armor is not an instance of Cloth
     */
    @Override
    public void equipArmor(Armor armor) {
        if(armor instanceof  Cloth){
            super.setArmor(armor);
        }else {
            throw new IllegalArgumentException("Required params: armor instance of Cloth. Received: " + armor.getClass());
        }

    }

    /**
     * Equip weapon to the character. Weapons are later used for calculating damage amount.
     * @param weapon - must be of MagicWeapon
     * @throws IllegalArgumentException - if the weapon is not an instance of MagicWeapon
     */
    public void equipWeapon(Weapon weapon) throws IllegalArgumentException {
        if ( weapon instanceof MagicWeapon ) {
            super.setWeapon( weapon );
        } else {
            throw new IllegalArgumentException("Required param: weapon instance of MagicWeapon. Recieved: " + weapon.getClass());
        }
    }
    // Character behaviours
}
