package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.PhysicalDamageDealer;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Mail;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends PhysicalDamageDealer {

    // Constructor
    public Ranger(String heroName) {
        super(CharacterBaseStatsDefensive.RANGER_BASE_HEALTH,
                CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER,
                heroName);
        equipArmor( new ArmorFactory().getArmor(ArmorType.Mail, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Bow));
    }

    public Ranger() {
        super(CharacterBaseStatsDefensive.RANGER_BASE_HEALTH,
                CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER,
                "Katniss Everdeen");
        equipArmor( new ArmorFactory().getArmor(ArmorType.Mail, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Bow));
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, used for calculating damage taken.
     * @param armor - Mail armor.
     * @throws IllegalArgumentException - if the armor is not an instance of Mail
     */
    @Override
    public void equipArmor(Armor armor) {
        //Mail
        if ( armor instanceof Mail) {
            super.setArmor(armor);
        } else {
            throw new IllegalArgumentException("Required param: armor instance of Mail. Received:" + armor.getClass());
        }

    }


    /**
     * Equip weapon to the character. Weapons are later used for calculating damage amount.
     * @param weapon - must be of RangedWeapon
     * @throws IllegalArgumentException - if the weapon is not an instance of RangedWeapon
     */
    public void equipWeapon(Weapon weapon) {
        if ( weapon instanceof RangedWeapon ) {
            super.setWeapon( weapon );
        } else {
            throw new IllegalArgumentException("Required param: weapon instance of RangedWeapon. Received:" + weapon.getClass());
        }

    }

}
