package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.PhysicalDamageDealer;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BladeWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

/*
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior extends PhysicalDamageDealer {

    // Constructor
    public Warrior(String heroName) {
        super(CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER,
                heroName);
        equipArmor( new ArmorFactory().getArmor(ArmorType.Plate, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Sword));
        // When the character is created it has maximum health (base health)
    }

    public Warrior() {
        super(CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER,
                "Sir Robin the Not-Quite-So-Brave");
        equipArmor( new ArmorFactory().getArmor(ArmorType.Plate, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Sword));
        // When the character is created it has maximum health (base health)
    }


    // Equipment behaviours

    /**
     * Equips armor to the character, used for calculating damage taken.
     * @param armor - Plate armor.
     * @throws IllegalArgumentException - if the armor is not an instance of Plate
     */
    public void equipArmor(Armor armor) {
        if (armor instanceof Plate ) {
            super.setArmor(armor);
        } else {
            throw new IllegalArgumentException("Required param: armor instance of Plate. Received:" + armor.getClass());
        }

    }

    /**
     * Equip weapon to the character. Weapons are later used for calculating damage amount.
     * @param weapon - must be of BladedWeapon.
     * @throws IllegalArgumentException - if the weapon is not an instance of BladedWeapon.
     */
    public void equipWeapon(Weapon weapon) {
        if ( weapon instanceof BladeWeapon ) {
            super.setWeapon( weapon );
        } else {
            throw new IllegalArgumentException("Required param: weapon instance of BladedWeapon. Received:" + weapon.getClass());
        }

    }

}
