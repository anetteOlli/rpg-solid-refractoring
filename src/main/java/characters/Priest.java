package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.ShieldingSupport;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Cloth;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.ShieldingSpell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends ShieldingSupport {


    // Constructor
    public Priest() {
        super(CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH,
                CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES,
                "Pope Benedick II");
        equipArmor( new ArmorFactory().getArmor(ArmorType.Cloth, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Wand));

    }

    public Priest(ShieldingSpell shieldingSpell, String heroName) {
        super(CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH,
                CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES,
                shieldingSpell,
                heroName);
        equipArmor( new ArmorFactory().getArmor(ArmorType.Cloth, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Wand));

    }
    public Priest(ShieldingSpell shieldingSpell) {
        super(CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH,
                CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES,
                shieldingSpell,
                "Pope Benedick II");
        equipArmor( new ArmorFactory().getArmor(ArmorType.Cloth, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Wand));
    }



    // Equipment behaviours

    /**
     * Equips armor to the character, used for calculating damage taken.
     * @param armor - Cloth armor.
     * @throws IllegalArgumentException - if the armor is not an instance of Cloth
     */
    @Override
    public void equipArmor(Armor armor) {
        //Cloth
        if ( armor instanceof Cloth) {
            super.setArmor(armor);
        } else {
            throw new IllegalArgumentException("Required param: armor instance of Cloth. Received:" + armor.getClass());
        }

    }


    /**
     * Equip weapon to the character. Weapons are later used for calculating healing amount.
     * @param weapon - must be of MagicWeapon.
     * @throws IllegalArgumentException - if the weapon is not an instance of MagicWeapon.
     */
    public void equipWeapon(Weapon weapon) {
        if ( weapon instanceof MagicWeapon ) {
            super.setWeapon( weapon );
        } else {
            throw new IllegalArgumentException("Required param: weapon instance of MagicWeapon. Received:" + weapon.getClass());
        }
    }


}
