package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CasterDamageDealer;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Cloth;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.spells.abstractions.DamagingSpell;

/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends CasterDamageDealer {

    // Constructor
    public Warlock() {
        super(CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER,
                "Albus Percival Wulfric Brian Dumbledore");
        equipArmor( new ArmorFactory().getArmor(ArmorType.Cloth, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Wand));
    }

    public Warlock( DamagingSpell damagingSpell, String heroName) {
        super(CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER,
                heroName,
                damagingSpell);
        equipArmor( new ArmorFactory().getArmor(ArmorType.Cloth, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Wand));
    }

    public Warlock( DamagingSpell damagingSpell) {
        super(CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER,
                "Albus Percival Wulfric Brian Dumbledore",
                damagingSpell);
        equipArmor( new ArmorFactory().getArmor(ArmorType.Cloth, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Wand));
    }


    // Equipment behaviours

    /**
     * Equips armor to the character, used for calculating damage taken.
     * @param armor - Cloth armor.
     * @throws IllegalArgumentException - if the armor is not an instance of Cloth
     */
    public void equipArmor(Armor armor) {
        if ( armor instanceof Cloth) {
            super.setArmor(armor);
        } else {
            throw new IllegalArgumentException("Required param: armor instance of Cloth. Received:" + armor.getClass());
        }
    }

    /**
     * Equip weapon to the character. Weapons are later used for calculating damage amount.
     * @param weapon - must be of MagicWeapon
     * @throws IllegalArgumentException - if the weapon is not an instance of MagicWeapon
     */
    public void equipWeapon(Weapon weapon) {
        if ( weapon instanceof MagicWeapon ) {
            super.setWeapon( weapon );
        } else {
            throw new IllegalArgumentException("Required param: weapon instance of MagicWeapon. Received:" + weapon.getClass());
        }
    }


    // Character behaviours


}
