package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.PhysicalDamageDealer;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/

public class Paladin extends PhysicalDamageDealer {

    // Constructor
    public Paladin(String heroName) {
        super(CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH,
                CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER,
                heroName);
        equipArmor( new ArmorFactory().getArmor(ArmorType.Plate, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Hammer));
    }

    public Paladin() {
        super(CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH,
                CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER,
                "Sir Plus");
        equipArmor( new ArmorFactory().getArmor(ArmorType.Plate, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Hammer));
    }

    // Equipment behaviours

    /**
     * Equips armor to the character, used for calculating damage taken.
     * @param armor - Plate armor.
     * @throws IllegalArgumentException - if the armor is not an instance of Plate
     */
    @Override
    public void equipArmor(Armor armor) {
        //Plate
        if ( armor instanceof Plate ) {
            super.setArmor(armor);
        } else {
            throw new IllegalArgumentException("Required param: armor instance of Plate. Received:" + armor.getClass());
        }

    }

    /**
     * Equip weapon to the character. Weapons are later used for calculating damage amount.
     * @param weapon - must be of BluntWeapon.
     * @throws IllegalArgumentException - if the weapon is not an instance of BluntWeapon.
     */
    public void equipWeapon(Weapon weapon) {
        if ( weapon instanceof BluntWeapon ) {
            super.setWeapon( weapon );
        } else {
            throw new IllegalArgumentException("Required param: weapon instance of BluntWeapon. Received:" + weapon.getClass());
        }
    }
}
