package main.java.characters;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.PhysicalDamageDealer;
import main.java.factories.ArmorFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.Leather;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Common;
import main.java.items.weapons.abstractions.BladeWeapon;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;

/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends PhysicalDamageDealer {

    // Constructor
    public Rogue(String heroName) {
        super(CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH,
                CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER,
                heroName);
        equipArmor( new ArmorFactory().getArmor(ArmorType.Leather, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Dagger));

    }

    public Rogue() {
        super(CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH,
                CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER,
                "Han Solo");
        equipArmor( new ArmorFactory().getArmor(ArmorType.Leather, new Common()));
        equipWeapon( new WeaponFactory().getCommonWeapon( WeaponType.Dagger));

    }


    // Equipment behaviours

    /**
     * Equips armor to the character, used for calculating damage taken.
     * @param armor - Leather armor.
     * @throws IllegalArgumentException - if the armor is not an instance of Leather
     */
    public void equipArmor(Armor armor) {
        //Leather
        if ( armor instanceof Leather) {
            super.setArmor(armor);
        } else {
            throw new IllegalArgumentException("Required param: armor instance of Leather. Received:" + armor.getClass());
        }
    }

    /**
     * Equip weapon to the character. Weapons are later used for calculating damage amount.
     * @param weapon - must be of BladedWeapon.
     * @throws IllegalArgumentException - if the weapon is not an instance of BladedWeapon.
     */
    public void equipWeapon(Weapon weapon) {
        if ( weapon instanceof BladeWeapon ) {
            super.setWeapon( weapon );
        } else {
            throw new IllegalArgumentException("Required param: weapon instance of BladedWeapon. Received:" + weapon.getClass());
        }
    }

}
