package main.java;
//imports:
import main.java.characters.Druid;
import main.java.characters.Mage;
import main.java.characters.Paladin;
import main.java.characters.abstractions.*;
import main.java.factories.ArmorFactory;
import main.java.factories.CharacterFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.rarity.Legendary;
import main.java.items.weapons.abstractions.WeaponType;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        CharacterFactory characterFactory = new CharacterFactory();
        ArrayList<Hero> partyList = new ArrayList<Hero>();
        partyList.add(characterFactory.getCharacter(CharacterType.Druid));
        partyList.add(characterFactory.getCharacter(CharacterType.Mage ));
        partyList.add(characterFactory.getCharacter(CharacterType.Paladin));
        partyList.add(characterFactory.getCharacter(CharacterType.Priest));
        partyList.add(characterFactory.getCharacter(CharacterType.Ranger));
        partyList.add(characterFactory.getCharacter(CharacterType.Rogue));
        partyList.add(characterFactory.getCharacter(CharacterType.Warlock));
        partyList.add(characterFactory.getCharacter(CharacterType.Warrior));

        System.out.println("\n*** do physical and magical damage to everyone");
        partyList.forEach( hero -> {
            ((Mage) partyList.get(1)).castDamagingSpell(hero);
            ((Druid) partyList.get(0)).healPartyMember(hero);
            ((Paladin) partyList.get(2)).attackWithWeapon(hero);
            System.out.println();
        });

        System.out.println("*** Hero actions demo: ***");

        for(int i = 0; i < partyList.size(); i++) {
            Hero hero = partyList.get(i);
            int actionTargetIndex = (i +1) % 7;
            if ( hero instanceof PhysicalDamageDealer) {
                ((PhysicalDamageDealer) hero).attackWithWeapon(partyList.get(actionTargetIndex));
            } else if( hero instanceof HealingSupport ) {
                ((HealingSupport) hero).healPartyMember(partyList.get(actionTargetIndex));
            } else if (hero instanceof  CasterDamageDealer ) {
                ((CasterDamageDealer) hero).castDamagingSpell(partyList.get(actionTargetIndex));
            } else if (hero instanceof ShieldingSupport) {
                ((ShieldingSupport) hero).shieldPartyMember(partyList.get(actionTargetIndex));
            }
        }

        System.out.println("\n*** Equip legendary Gun ***");
        WeaponFactory weaponFactory = new WeaponFactory();

        partyList.forEach( hero -> {
            try {
                hero.equipWeapon(weaponFactory.getItem(WeaponType.Gun, new Legendary()));
                System.out.println(hero.getClass() + " equipped legendary Gun");
            }catch (IllegalArgumentException e) {
                System.out.println(hero.getClass() + " can't equip Gun");
            }
        });

        ArmorFactory armorFactory = new ArmorFactory();

        System.out.println("\n*** Equip legendary Cloth armor");
        partyList.forEach( hero -> {
            try {
                hero.equipArmor(armorFactory.getArmor(ArmorType.Cloth, new Legendary()));
                System.out.println(hero.getClass() + " equipped cloth armor");
            }catch (IllegalArgumentException e) {
                System.out.println(hero.getClass() + " can't equip Cloth armor");
            }
        });

        System.out.println("\n*** new rounds of actions ***");
        for(int i = 0; i < partyList.size(); i++) {
            Hero hero = partyList.get(i);
            int actionTargetIndex = (i +2) % 7;
            if ( hero instanceof PhysicalDamageDealer) {
                ((PhysicalDamageDealer) hero).attackWithWeapon(partyList.get(actionTargetIndex));
            } else if( hero instanceof HealingSupport ) {
                ((HealingSupport) hero).healPartyMember(partyList.get(actionTargetIndex));
            } else if (hero instanceof  CasterDamageDealer ) {
                ((CasterDamageDealer) hero).castDamagingSpell(partyList.get(actionTargetIndex));
            } else if (hero instanceof ShieldingSupport) {
                ((ShieldingSupport) hero).shieldPartyMember(partyList.get(actionTargetIndex));
            }
        }
    }
}
